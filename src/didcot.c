/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */


#include "didcot-internal.h"
#include <string.h>

static void init_resources(void);
static void on_bus_acquired (GDBusConnection *connection,const gchar *name,gpointer         user_data);
static void on_name_acquired (GDBusConnection *connection,const gchar *name,gpointer         user_data);
static void on_name_lost (GDBusConnection *connection,const gchar *name,gpointer user_data);

static void on_bus_acquired (GDBusConnection *connection,
                             const gchar     *name,
                             gpointer         user_data)
{
  app_launch_interface_initialize (connection);

}

static void on_name_acquired (GDBusConnection *connection,
                  	  	  	  const gchar     *name,
                  	  	  	  gpointer         user_data)
{
     DIDCOT_DEBUG ("enter");

}

static void
on_name_lost (GDBusConnection *connection,
              const gchar     *name,
              gpointer         user_data)
{

  DIDCOT_DEBUG ("enter");
}

static void
init_resources (void)
{
  data_struct.popup_proxy = NULL;
  data_struct.request_list = g_ptr_array_new ();
}

gint main(gint argc, gchar *argv[])
{

  GMainLoop *mainloop = NULL;

    /* Initialise all resources for the data exchange manager */
	init_resources();

	popup_client_handler_init();

    app_launch_init ();

    g_bus_own_name (G_BUS_TYPE_SESSION,
                    "org.apertis.Didcot",
                    G_BUS_NAME_OWNER_FLAGS_NONE,
                    on_bus_acquired,
                    on_name_acquired,
                    on_name_lost,
                    NULL,
                    NULL);




  mainloop = g_main_loop_new (NULL, FALSE);

  g_main_loop_run (mainloop);

  g_clear_object (&data_struct.entry_point_index);
  g_clear_pointer (&data_struct.request_list, g_ptr_array_unref);
  exit (0);
}
